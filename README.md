# SMSI Examples

These examples show the algorithm of the SMSI Lecture.
Their implementations can be found [here](https://gitlab.com/griebel/smsi.jl).

To start the notebooks use:

```julia
julia> import Pluto
julia> Pluto.run()
```
