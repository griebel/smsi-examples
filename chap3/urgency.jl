### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ c1747b50-04f7-11ec-08b7-f1d51f20ac17
begin
	import Pkg
	Pkg.activate(mktempdir())
	Pkg.add("Colors")
	Pkg.add("GraphPlot")
	Pkg.add("LightGraphs")
	Pkg.add("MetaGraphs")
	Pkg.add(url="https://gitlab.com/griebel/smsi.jl")
	using Colors
	using GraphPlot
	using LightGraphs
	using MetaGraphs
	using SMSI
end

# ╔═╡ 1fd8c949-bbdd-4983-b58c-a16a0f778dd6
md"""
# Urgency Yield 
## Graph
"""

# ╔═╡ b01eaddf-4b4e-4346-8250-a6cb2b9bea7f
begin
	dfg = MetaDiGraph(5)
    set_props!(dfg, 1, Dict(:type => :multiplier))
    set_props!(dfg, 2, Dict(:type => :adder))
    set_props!(dfg, 3, Dict(:type => :adder))
    set_props!(dfg, 4, Dict(:type => :adder))
    set_props!(dfg, 5, Dict(:type => :multiplier))
    add_edge!(dfg, 1, 4)
    add_edge!(dfg, 2, 4)
    add_edge!(dfg, 4, 5)
    add_edge!(dfg, 3, 5)
	
	dfg
end

# ╔═╡ b027b82c-7c42-4104-b426-c7975726036e
delta = Dict(:adder => 1, :multiplier => 2)

# ╔═╡ e7773876-f5ff-4fe0-902e-f11fd97bf19b
color = Dict(:adder => colorant"lightblue", :multiplier => colorant"lightyellow")

# ╔═╡ b7b0f7bb-fd7b-4c8c-b16b-1662ed1b9ca1
begin
	nodelabel_0 = Vector{String}(undef, nv(dfg))
	nodefillc_0 = Vector{RGB}(undef, nv(dfg))
	for i in 1:nv(dfg)
		prop = get_prop(dfg, i, :type)
		nodelabel_0[i] = "O" * string(i)
		nodefillc_0[i] = color[prop]
	end
	gplot(dfg, nodefillc=nodefillc_0, nodelabel=nodelabel_0)
end

# ╔═╡ 2fca7fe1-b521-4c78-ae96-71f261b01f67
md"""
## Priority
"""

# ╔═╡ ae95423a-932b-43dc-8fcb-1be7476a0a3a
priority = urgency(dfg, delta)

# ╔═╡ 1ab91b38-fc4b-4dfc-92a0-20ff0aa0a669
begin
	nodelabel_1 = Vector{String}(undef, nv(dfg))
	nodefillc_1 = Vector{RGB}(undef, nv(dfg))
	for i in 1:nv(dfg)
		prop = get_prop(dfg, i, :type)
		nodelabel_1[i] = "O" * string(i)* ", P" * string(priority[i])
		nodefillc_1[i] = color[prop]
	end
	gplot(dfg, nodefillc=nodefillc_1, nodelabel=nodelabel_1)
end

# ╔═╡ Cell order:
# ╟─c1747b50-04f7-11ec-08b7-f1d51f20ac17
# ╟─1fd8c949-bbdd-4983-b58c-a16a0f778dd6
# ╟─b01eaddf-4b4e-4346-8250-a6cb2b9bea7f
# ╟─b027b82c-7c42-4104-b426-c7975726036e
# ╟─e7773876-f5ff-4fe0-902e-f11fd97bf19b
# ╟─b7b0f7bb-fd7b-4c8c-b16b-1662ed1b9ca1
# ╟─2fca7fe1-b521-4c78-ae96-71f261b01f67
# ╠═ae95423a-932b-43dc-8fcb-1be7476a0a3a
# ╟─1ab91b38-fc4b-4dfc-92a0-20ff0aa0a669
