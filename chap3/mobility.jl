### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ 52413586-04f5-11ec-363b-69f5e1dee688
begin
	import Pkg
	Pkg.activate(mktempdir())
	Pkg.add("Colors")
	Pkg.add("GraphPlot")
	Pkg.add("LightGraphs")
	Pkg.add("MetaGraphs")
	Pkg.add(url="https://gitlab.com/griebel/smsi.jl")
	using Colors
	using GraphPlot
	using LightGraphs
	using MetaGraphs
	using SMSI
end

# ╔═╡ 985df74b-7e2a-4a92-8ad5-f7728c970375
md"""
# Mobility Yield 
## Graph
"""

# ╔═╡ 6d6dc331-2308-4f49-a251-12bc46ae29b2
begin
	dfg = MetaDiGraph(5)
    set_props!(dfg, 1, Dict(:type => :multiplier))
    set_props!(dfg, 2, Dict(:type => :adder))
    set_props!(dfg, 3, Dict(:type => :adder))
    set_props!(dfg, 4, Dict(:type => :adder))
    set_props!(dfg, 5, Dict(:type => :multiplier))
    add_edge!(dfg, 1, 4)
    add_edge!(dfg, 2, 4)
    add_edge!(dfg, 4, 5)
    add_edge!(dfg, 3, 5)
	
	dfg
end

# ╔═╡ c1c8355e-90f2-49c1-92d6-5a1f845fc890
delta = Dict(:adder => 1, :multiplier => 2)

# ╔═╡ 9c6a80fe-b13f-4c22-b1a7-4261c8969c74
color = Dict(:adder => colorant"lightblue", :multiplier => colorant"lightyellow")

# ╔═╡ c5d8328e-2d18-4e5c-af04-99d16474fdd1
begin
	nodelabel_0 = Vector{String}(undef, nv(dfg))
	nodefillc_0 = Vector{RGB}(undef, nv(dfg))
	for i in 1:nv(dfg)
		prop = get_prop(dfg, i, :type)
		nodelabel_0[i] = "O" * string(i)
		nodefillc_0[i] = color[prop]
	end
	gplot(dfg, nodefillc=nodefillc_0, nodelabel=nodelabel_0)
end

# ╔═╡ bd8c59ee-8ed4-4a45-89ac-b29bde0295a0
md"""
## Priority
"""

# ╔═╡ c5696605-9c9b-438a-b8f7-6f8b0dc5e527
priority = mobility(dfg, delta)

# ╔═╡ b9e01529-80f9-4da4-90b8-9e2ce6daca98
begin
	nodelabel_1 = Vector{String}(undef, nv(dfg))
	nodefillc_1 = Vector{RGB}(undef, nv(dfg))
	for i in 1:nv(dfg)
		prop = get_prop(dfg, i, :type)
		nodelabel_1[i] = "O" * string(i)* ", P" * string(priority[i])
		nodefillc_1[i] = color[prop]
	end
	gplot(dfg, nodefillc=nodefillc_1, nodelabel=nodelabel_1)
end

# ╔═╡ Cell order:
# ╟─52413586-04f5-11ec-363b-69f5e1dee688
# ╟─985df74b-7e2a-4a92-8ad5-f7728c970375
# ╟─6d6dc331-2308-4f49-a251-12bc46ae29b2
# ╟─c1c8355e-90f2-49c1-92d6-5a1f845fc890
# ╟─9c6a80fe-b13f-4c22-b1a7-4261c8969c74
# ╟─c5d8328e-2d18-4e5c-af04-99d16474fdd1
# ╟─bd8c59ee-8ed4-4a45-89ac-b29bde0295a0
# ╠═c5696605-9c9b-438a-b8f7-6f8b0dc5e527
# ╟─b9e01529-80f9-4da4-90b8-9e2ce6daca98
