### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ 46b0a2f8-05a4-11ec-380d-7147f4f6087b
begin
	import Pkg
	Pkg.activate(mktempdir())
	Pkg.add("Colors")
	Pkg.add("GraphPlot")
	Pkg.add("LightGraphs")
	Pkg.add(url="https://gitlab.com/griebel/smsi.jl")
	using Colors
	using GraphPlot
	using LightGraphs
	using SMSI
end

# ╔═╡ 50df9e73-d805-4cee-8233-ca5a85bf2b08
md"""
# Hu's List Scheduling
## Graph
"""

# ╔═╡ 677f83e8-e10e-4959-b0b1-7e2e8ff1f782
begin
	dfg = SimpleDiGraph(5)
    add_edge!(dfg, 1, 4)
    add_edge!(dfg, 2, 4)
    add_edge!(dfg, 4, 5)
    add_edge!(dfg, 3, 5)
	
	dfg
end

# ╔═╡ 93cea0c7-9efa-468a-a627-45f74420ac0e
begin
	nodelabel_0 = Vector{String}(undef, nv(dfg))
	for i in 1:nv(dfg)
		nodelabel_0[i] = "O" * string(i)
	end
	gplot(dfg, nodefillc=colorant"lightblue", nodelabel=nodelabel_0)
end

# ╔═╡ 8e08e9fb-0700-4d52-bfc6-6822602f49c4
priority = [3, 3, 2, 2, 1]

# ╔═╡ 3e484112-5a58-4733-bb88-8e289ee8cced
functional_elements = 2

# ╔═╡ 83c8c28d-7210-4093-bf70-55b6ce6d17d5
md"""
## Scheduling
"""

# ╔═╡ 4b4900dd-55ac-4655-92ee-04e08a4f9f7d
schedule = hus_list_scheduling(dfg, functional_elements, priority)

# ╔═╡ 19144510-1103-4806-a106-8d69e893a1fa
begin
	nodelabel_1 = Vector{String}(undef, nv(dfg))
	for i in 1:nv(dfg)
		nodelabel_1[i] = "O" * string(i)* ", CC" * string(schedule[i])
	end
	gplot(dfg, nodefillc=colorant"lightblue", nodelabel=nodelabel_1)
end

# ╔═╡ Cell order:
# ╟─46b0a2f8-05a4-11ec-380d-7147f4f6087b
# ╟─50df9e73-d805-4cee-8233-ca5a85bf2b08
# ╟─677f83e8-e10e-4959-b0b1-7e2e8ff1f782
# ╟─93cea0c7-9efa-468a-a627-45f74420ac0e
# ╟─8e08e9fb-0700-4d52-bfc6-6822602f49c4
# ╟─3e484112-5a58-4733-bb88-8e289ee8cced
# ╟─83c8c28d-7210-4093-bf70-55b6ce6d17d5
# ╠═4b4900dd-55ac-4655-92ee-04e08a4f9f7d
# ╟─19144510-1103-4806-a106-8d69e893a1fa
