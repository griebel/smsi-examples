### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ cbe5d100-0647-11ec-31fb-7da5c86d5b0e
begin
	import Pkg
	Pkg.activate(mktempdir())
	Pkg.add("Colors")
	Pkg.add("GraphPlot")
	Pkg.add("LightGraphs")
	Pkg.add("MetaGraphs")
	Pkg.add(url="https://gitlab.com/griebel/smsi.jl")
	using Colors
	using GraphPlot
	using LightGraphs
	using MetaGraphs
	using SMSI
end

# ╔═╡ dc7ec365-f13a-405b-b070-943197f4918a
md"""
# General List Scheduling
## Graph
"""

# ╔═╡ 427a5a84-bd91-4e0d-bcc1-1e2201ffb57b
begin
    dfg = MetaDiGraph(5, 1)
    set_props!(dfg, 1, Dict(:type => :multiplier))
    set_props!(dfg, 2, Dict(:type => :adder))
    set_props!(dfg, 3, Dict(:type => :adder))
    set_props!(dfg, 4, Dict(:type => :adder))
    set_props!(dfg, 5, Dict(:type => :multiplier))
    add_edge!(dfg, 1, 4)
    add_edge!(dfg, 2, 4)
    add_edge!(dfg, 4, 5)
    add_edge!(dfg, 3, 5)
	
	dfg
end

# ╔═╡ c0b95228-920a-4b3b-967a-d348a8517382
functional_elements = Dict(:adder => 1, :multiplier => 1)

# ╔═╡ a9158668-7b95-40b4-8ed2-d6b01afc1d9a
priority = [3, 3, 2, 2, 1]

# ╔═╡ 67f2c737-c04b-4e2f-924d-cec9da3ad253
color = Dict(:adder => colorant"lightblue", :multiplier => colorant"lightyellow")

# ╔═╡ 7122d24e-7f18-4b83-ba84-c39c9ab4fda1
begin
	nodelabel_0 = Vector{String}(undef, nv(dfg))
	nodefillc_0 = Vector{RGB}(undef, nv(dfg))
	for i in 1:nv(dfg)
		prop = get_prop(dfg, i, :type)
		nodelabel_0[i] = "O" * string(i)
		nodefillc_0[i] = color[prop]
	end
	gplot(dfg, nodefillc=nodefillc_0, nodelabel=nodelabel_0)
end

# ╔═╡ c7ce6ed3-07fd-4d3f-b9f9-ee0d3ae2b00c
md"""
## Scheduling
"""

# ╔═╡ 3f4c67ef-8895-4092-b387-5c54873109a7
schedule = general_list_scheduling(dfg, functional_elements, priority)

# ╔═╡ 5eb73ba8-3775-4446-89e5-24b6ab1e4957
begin
	nodelabel_1 = Vector{String}(undef, nv(dfg))
	nodefillc_1 = Vector{RGB}(undef, nv(dfg))
	for i in 1:nv(dfg)
		prop = get_prop(dfg, i, :type)
		nodelabel_1[i] = "O" * string(i)* ", CC" * string(schedule[i])
		nodefillc_1[i] = color[prop]
	end
	gplot(dfg, nodefillc=nodefillc_1, nodelabel=nodelabel_1)
end

# ╔═╡ Cell order:
# ╟─cbe5d100-0647-11ec-31fb-7da5c86d5b0e
# ╟─dc7ec365-f13a-405b-b070-943197f4918a
# ╟─427a5a84-bd91-4e0d-bcc1-1e2201ffb57b
# ╟─c0b95228-920a-4b3b-967a-d348a8517382
# ╟─a9158668-7b95-40b4-8ed2-d6b01afc1d9a
# ╟─67f2c737-c04b-4e2f-924d-cec9da3ad253
# ╟─7122d24e-7f18-4b83-ba84-c39c9ab4fda1
# ╟─c7ce6ed3-07fd-4d3f-b9f9-ee0d3ae2b00c
# ╠═3f4c67ef-8895-4092-b387-5c54873109a7
# ╟─5eb73ba8-3775-4446-89e5-24b6ab1e4957
