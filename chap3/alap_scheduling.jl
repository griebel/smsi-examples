### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ a647a546-04f2-11ec-2d7c-e3f0ce69a95c
begin
	import Pkg
	Pkg.activate(mktempdir())
	Pkg.add("Colors")
	Pkg.add("GraphPlot")
	Pkg.add("LightGraphs")
	Pkg.add("MetaGraphs")
	Pkg.add(url="https://gitlab.com/griebel/smsi.jl")
	using Colors
	using GraphPlot
	using LightGraphs
	using MetaGraphs
	using SMSI
end

# ╔═╡ d0dce10e-9cbc-48f0-a9c5-fc195ef6092b
md"""
# ALAP Scheduling
## Graph
"""

# ╔═╡ 8d436258-d1a1-413b-a869-18fc758ed43f
begin
	dfg = MetaDiGraph(5)
    set_props!(dfg, 1, Dict(:type => :multiplier))
    set_props!(dfg, 2, Dict(:type => :adder))
    set_props!(dfg, 3, Dict(:type => :adder))
    set_props!(dfg, 4, Dict(:type => :adder))
    set_props!(dfg, 5, Dict(:type => :multiplier))
    add_edge!(dfg, 1, 4)
    add_edge!(dfg, 2, 4)
    add_edge!(dfg, 4, 5)
    add_edge!(dfg, 3, 5)
	
	dfg
end

# ╔═╡ 6a63d3ef-5d44-4f4b-be7c-52386b3d6b34
delta = Dict(:adder => 1, :multiplier => 2)

# ╔═╡ 014a72cc-3e10-4df2-9997-c3b88a07195b
color = Dict(:adder => colorant"lightblue", :multiplier => colorant"lightyellow")

# ╔═╡ a4878633-072d-45c5-b253-22980d99e4b9
begin
	nodelabel_0 = Vector{String}(undef, nv(dfg))
	nodefillc_0 = Vector{RGB}(undef, nv(dfg))
	for i in 1:nv(dfg)
		prop = get_prop(dfg, i, :type)
		nodelabel_0[i] = "O" * string(i)
		nodefillc_0[i] = color[prop]
	end
	gplot(dfg, nodefillc=nodefillc_0, nodelabel=nodelabel_0)
end

# ╔═╡ b8630b60-c43f-4ab9-839d-0771504b8004
md"""
## Scheduling
"""

# ╔═╡ 168abb20-7e12-42ae-9f2b-a2fd920fcd11
schedule = alap_scheduling(dfg, delta)

# ╔═╡ 09fe0965-ed8b-4907-b22e-9a6c0a9cbc7d
begin
	nodelabel_1 = Vector{String}(undef, nv(dfg))
	nodefillc_1 = Vector{RGB}(undef, nv(dfg))
	for i in 1:nv(dfg)
		prop = get_prop(dfg, i, :type)
		nodelabel_1[i] = "O" * string(i)* ", CC" * string(schedule[i])
		nodefillc_1[i] = color[prop]
	end
	gplot(dfg, nodefillc=nodefillc_1, nodelabel=nodelabel_1)
end

# ╔═╡ Cell order:
# ╟─a647a546-04f2-11ec-2d7c-e3f0ce69a95c
# ╟─d0dce10e-9cbc-48f0-a9c5-fc195ef6092b
# ╟─8d436258-d1a1-413b-a869-18fc758ed43f
# ╟─6a63d3ef-5d44-4f4b-be7c-52386b3d6b34
# ╟─014a72cc-3e10-4df2-9997-c3b88a07195b
# ╟─a4878633-072d-45c5-b253-22980d99e4b9
# ╟─b8630b60-c43f-4ab9-839d-0771504b8004
# ╠═168abb20-7e12-42ae-9f2b-a2fd920fcd11
# ╟─09fe0965-ed8b-4907-b22e-9a6c0a9cbc7d
