### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ fb77be72-04e1-11ec-2da0-6df51d8d3807
begin
	import Pkg
	Pkg.activate(mktempdir())
	Pkg.add("Colors")
	Pkg.add("GraphPlot")
	Pkg.add("LightGraphs")
	Pkg.add("MetaGraphs")
	Pkg.add(url="https://gitlab.com/griebel/smsi.jl")
	using Colors
	using GraphPlot
	using LightGraphs
	using MetaGraphs
	using SMSI
end

# ╔═╡ 8df752a3-c5ef-40cd-afbd-cfff5244ddf2
md"""
# ASAP Scheduling
## Graph
"""

# ╔═╡ 2a112e84-97c4-4541-8b7a-10e0c76e1033
begin
	dfg = MetaDiGraph(5)
    set_props!(dfg, 1, Dict(:type => :multiplier))
    set_props!(dfg, 2, Dict(:type => :adder))
    set_props!(dfg, 3, Dict(:type => :adder))
    set_props!(dfg, 4, Dict(:type => :adder))
    set_props!(dfg, 5, Dict(:type => :multiplier))
    add_edge!(dfg, 1, 4)
    add_edge!(dfg, 2, 4)
    add_edge!(dfg, 4, 5)
    add_edge!(dfg, 3, 5)
	
	dfg
end

# ╔═╡ ab5e71bc-f64e-4bf5-b27b-d74c2e6d726b
delta = Dict(:adder => 1, :multiplier => 2)

# ╔═╡ 81461713-9b57-4267-be45-4bc055fa070b
color = Dict(:adder => colorant"lightblue", :multiplier => colorant"lightyellow")

# ╔═╡ fb3d0956-d4ac-4b22-876d-f2dc4c4ad218
begin
	nodelabel_0 = Vector{String}(undef, nv(dfg))
	nodefillc_0 = Vector{RGB}(undef, nv(dfg))
	for i in 1:nv(dfg)
		prop = get_prop(dfg, i, :type)
		nodelabel_0[i] = "O" * string(i)
		nodefillc_0[i] = color[prop]
	end
	gplot(dfg, nodefillc=nodefillc_0, nodelabel=nodelabel_0)
end

# ╔═╡ 3fee4789-b25e-47ac-a42e-432aeebe16e4
md"""
## Scheduling
"""

# ╔═╡ c3926249-8343-4621-ab7b-c286e6b36d01
schedule = asap_scheduling(dfg, delta)

# ╔═╡ 27dc3730-869a-4692-8974-2bb9aa954fa1
begin
	nodelabel_1 = Vector{String}(undef, nv(dfg))
	nodefillc_1 = Vector{RGB}(undef, nv(dfg))
	for i in 1:nv(dfg)
		prop = get_prop(dfg, i, :type)
		nodelabel_1[i] = "O" * string(i)* ", CC" * string(schedule[i])
		nodefillc_1[i] = color[prop]
	end
	gplot(dfg, nodefillc=nodefillc_1, nodelabel=nodelabel_1)
end

# ╔═╡ Cell order:
# ╟─fb77be72-04e1-11ec-2da0-6df51d8d3807
# ╟─8df752a3-c5ef-40cd-afbd-cfff5244ddf2
# ╟─2a112e84-97c4-4541-8b7a-10e0c76e1033
# ╟─ab5e71bc-f64e-4bf5-b27b-d74c2e6d726b
# ╟─81461713-9b57-4267-be45-4bc055fa070b
# ╟─fb3d0956-d4ac-4b22-876d-f2dc4c4ad218
# ╟─3fee4789-b25e-47ac-a42e-432aeebe16e4
# ╠═c3926249-8343-4621-ab7b-c286e6b36d01
# ╟─27dc3730-869a-4692-8974-2bb9aa954fa1
