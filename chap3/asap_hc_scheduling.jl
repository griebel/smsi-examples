### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ d390d5f6-04eb-11ec-072c-7d55a5a09031
begin
	import Pkg
	Pkg.activate(mktempdir())
	Pkg.add("Colors")
	Pkg.add("GraphPlot")
	Pkg.add("LightGraphs")
	Pkg.add("MetaGraphs")
	Pkg.add(url="https://gitlab.com/griebel/smsi.jl")
	using Colors
	using GraphPlot
	using LightGraphs
	using MetaGraphs
	using SMSI
end

# ╔═╡ 6cb1c664-fa50-42e2-8704-395d479bd4bc
md"""
# ASAP Scheduling with Hardware Constraints
## Graph
"""

# ╔═╡ db709435-b651-4144-a90c-9cdb79a581dc
begin
	dfg = MetaDiGraph(5)
    set_props!(dfg, 1, Dict(:type => :multiplier))
    set_props!(dfg, 2, Dict(:type => :adder))
    set_props!(dfg, 3, Dict(:type => :adder))
    set_props!(dfg, 4, Dict(:type => :adder))
    set_props!(dfg, 5, Dict(:type => :multiplier))
    add_edge!(dfg, 1, 4)
    add_edge!(dfg, 2, 4)
    add_edge!(dfg, 4, 5)
    add_edge!(dfg, 3, 5)
	
	dfg
end

# ╔═╡ 83c63cc7-2740-4d96-afd1-13fed21def8f
delta = Dict(:adder => 1, :multiplier => 2)

# ╔═╡ 3a68c62e-094e-4088-b4cc-1211fcc8a70b
tau = Dict(:adder => 1, :multiplier => 1)

# ╔═╡ 4a2295a0-1c30-4bc2-8f3a-0d3232c72531
color = Dict(:adder => colorant"lightblue", :multiplier => colorant"lightyellow")

# ╔═╡ 28e432d7-ce43-4fe7-bff0-f81415f4f9e7
begin
	nodelabel_0 = Vector{String}(undef, nv(dfg))
	nodefillc_0 = Vector{RGB}(undef, nv(dfg))
	for i in 1:nv(dfg)
		prop = get_prop(dfg, i, :type)
		nodelabel_0[i] = "O" * string(i)
		nodefillc_0[i] = color[prop]
	end
	gplot(dfg, nodefillc=nodefillc_0, nodelabel=nodelabel_0)
end

# ╔═╡ 9b6070a9-b9a4-48cf-bb65-f5c968f4bede
md"""
## Scheduling
"""

# ╔═╡ e99e6d54-cb82-4a5b-9787-420f5365bd1d
schedule = asap_hc_scheduling(dfg, delta, tau)

# ╔═╡ f8654bf9-3905-46b8-b839-085d3d209494
begin
	nodelabel_1 = Vector{String}(undef, nv(dfg))
	nodefillc_1 = Vector{RGB}(undef, nv(dfg))
	for i in 1:nv(dfg)
		prop = get_prop(dfg, i, :type)
		nodelabel_1[i] = "O" * string(i)* ", CC" * string(schedule[i])
		nodefillc_1[i] = color[prop]
	end
	gplot(dfg, nodefillc=nodefillc_1, nodelabel=nodelabel_1)
end

# ╔═╡ Cell order:
# ╟─d390d5f6-04eb-11ec-072c-7d55a5a09031
# ╟─6cb1c664-fa50-42e2-8704-395d479bd4bc
# ╟─db709435-b651-4144-a90c-9cdb79a581dc
# ╟─83c63cc7-2740-4d96-afd1-13fed21def8f
# ╟─3a68c62e-094e-4088-b4cc-1211fcc8a70b
# ╟─4a2295a0-1c30-4bc2-8f3a-0d3232c72531
# ╟─28e432d7-ce43-4fe7-bff0-f81415f4f9e7
# ╟─9b6070a9-b9a4-48cf-bb65-f5c968f4bede
# ╠═e99e6d54-cb82-4a5b-9787-420f5365bd1d
# ╟─f8654bf9-3905-46b8-b839-085d3d209494
