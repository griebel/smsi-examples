### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ 4b3c661e-04cf-11ec-033c-6df32198016a
begin
	import Pkg
	Pkg.activate(mktempdir())
	Pkg.add("GLMakie")
	Pkg.add(url="https://gitlab.com/griebel/smsi.jl")
	using GLMakie
	using SMSI
end

# ╔═╡ 0789b9b2-14ea-43e0-824b-31954bf9040f
md"""
# Threshold Acceptance
## Initial path
"""

# ╔═╡ 91823985-d5f0-44c7-8d62-74c9120342aa
path = [
        (200, 40),
        (60, 80),
        (100, 40),
        (180, 60),
        (20, 20),
        (180, 200),
        (120, 80),
        (60, 200),
        (20, 40),
        (200, 160),
        (140, 140),
        (60, 20),
        (160, 20),
        (140, 180),
        (180, 100),
        (20, 160),
        (80, 180),
        (40, 120),
    ]

# ╔═╡ 3b1a8182-3f58-4617-9a90-0b07490e1037
begin
	fig1 = Figure()
    ax1 = Axis(fig1[1, 1])
    scatter!(ax1, path, color=:red)
    lines!(ax1, path, color=:blue)
    lines!(ax1, [path[end], path[1]], color=:blue)

    fig1
end

# ╔═╡ 43f29663-e6da-42a5-ae7b-f5b023efaaea
cost = distance(path)

# ╔═╡ 4e41d3aa-da5c-47cb-8097-f76142385612
md"""
## Optimize
"""

# ╔═╡ c5c61e27-f5e9-4e06-9d40-6946c7490803
opt_cost, opt_path = threshold_acceptance(path, 0.1, 2500)

# ╔═╡ d3991e05-0795-4e0d-aa29-6c59c2b1e19b
begin
	fig2 = Figure()
    ax2 = Axis(fig2[1, 1])
    scatter!(ax2, opt_path, color=:red)
    lines!(ax2, opt_path, color=:blue)
    lines!(ax2, [opt_path[end], opt_path[1]], color=:blue)

    fig2
end

# ╔═╡ Cell order:
# ╟─4b3c661e-04cf-11ec-033c-6df32198016a
# ╟─0789b9b2-14ea-43e0-824b-31954bf9040f
# ╟─91823985-d5f0-44c7-8d62-74c9120342aa
# ╟─3b1a8182-3f58-4617-9a90-0b07490e1037
# ╠═43f29663-e6da-42a5-ae7b-f5b023efaaea
# ╟─4e41d3aa-da5c-47cb-8097-f76142385612
# ╠═c5c61e27-f5e9-4e06-9d40-6946c7490803
# ╟─d3991e05-0795-4e0d-aa29-6c59c2b1e19b
