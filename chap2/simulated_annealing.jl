### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ b94c41d6-04bd-11ec-14ac-bb97fef411a2
begin
	import Pkg
	Pkg.activate(mktempdir())
	Pkg.add("GLMakie")
	Pkg.add(url="https://gitlab.com/griebel/smsi.jl")
	using GLMakie
	using SMSI
end

# ╔═╡ 2abe2d44-8bd2-48c6-a856-a8dc10a3c2d5
md"""
# Simulated Annealing
## Initial path
"""

# ╔═╡ ab2a01ed-b733-4543-a179-f8b614bf0844
path = [
        (200, 40),
        (60, 80),
        (100, 40),
        (180, 60),
        (20, 20),
        (180, 200),
        (120, 80),
        (60, 200),
        (20, 40),
        (200, 160),
        (140, 140),
        (60, 20),
        (160, 20),
        (140, 180),
        (180, 100),
        (20, 160),
        (80, 180),
        (40, 120),
    ]

# ╔═╡ c3865e08-40bc-4e7d-84c4-d759f8684005
begin
	fig1 = Figure()
    ax1 = Axis(fig1[1, 1])
    scatter!(ax1, path, color=:red)
    lines!(ax1, path, color=:blue)
    lines!(ax1, [path[end], path[1]], color=:blue)

    fig1
end

# ╔═╡ e4cc73f5-e9e6-4c63-b8fc-7073aba26442
cost = distance(path)

# ╔═╡ f90cd57d-b8b7-4004-a9a9-a7dbeec7e516
md"""
## Optimize
"""

# ╔═╡ 5d30fd59-61f2-42e4-afe9-03105f2daf7f
opt_cost, opt_path = simulated_annealing(path, 0.000001, 0.99, 2500)

# ╔═╡ cb8d665d-4213-4535-8a10-5d6066d19b52
begin
	fig2 = Figure()
    ax2 = Axis(fig2[1, 1])
    scatter!(ax2, opt_path, color=:red)
    lines!(ax2, opt_path, color=:blue)
    lines!(ax2, [opt_path[end], opt_path[1]], color=:blue)

    fig2
end

# ╔═╡ Cell order:
# ╟─b94c41d6-04bd-11ec-14ac-bb97fef411a2
# ╟─2abe2d44-8bd2-48c6-a856-a8dc10a3c2d5
# ╟─ab2a01ed-b733-4543-a179-f8b614bf0844
# ╟─c3865e08-40bc-4e7d-84c4-d759f8684005
# ╠═e4cc73f5-e9e6-4c63-b8fc-7073aba26442
# ╟─f90cd57d-b8b7-4004-a9a9-a7dbeec7e516
# ╠═5d30fd59-61f2-42e4-afe9-03105f2daf7f
# ╟─cb8d665d-4213-4535-8a10-5d6066d19b52
