### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ d9043bb8-0400-11ec-38b4-5dd261f49788
begin
	import Pkg
	Pkg.activate(mktempdir())
	Pkg.add("Colors")
	Pkg.add("GraphPlot")
	Pkg.add("LightGraphs")
	Pkg.add(url="https://gitlab.com/griebel/smsi.jl")
	using Colors
	using GraphPlot
	using LightGraphs
	using SMSI
end

# ╔═╡ 0efecbfd-f255-4929-ac2c-1481a4ce8f8d
md"""
# Kernighan Lin Algorithm
## Problem
### Partitions
"""

# ╔═╡ c37d7cba-a331-42ff-bc54-a15d604737ec
V_1 = [1, 2, 3, 4, 14, 15, 16, 17, 18]

# ╔═╡ 78a868e8-93b3-47a4-a03c-5c5267e841bf
V_2 = [5, 6, 7, 8, 9, 10, 11, 12, 13]

# ╔═╡ 2b2a4d0c-76dd-4512-82f2-51f7d1c911bd
md"""
### Cut size
"""

# ╔═╡ 62ad82f1-6ce3-4bdb-917d-e28f6ae2e049
c_0 = 10

# ╔═╡ 0473c25d-37b7-4285-abc6-221124d2bdb4
md"""
### Graph
"""

# ╔═╡ 62c3753e-189f-470e-bf83-861c441f2901
begin
	S = SimpleGraph(18)

	add_edge!(S, 1, 2)
	add_edge!(S, 1, 3)
	add_edge!(S, 1, 4)
	add_edge!(S, 1, 5)
	
	add_edge!(S, 2, 3)
	add_edge!(S, 2, 4)
	add_edge!(S, 2, 5)
	
	add_edge!(S, 3, 4)
	add_edge!(S, 3, 6)
	add_edge!(S, 3, 10)
	
	add_edge!(S, 4, 6)
	add_edge!(S, 4, 11)
	
	add_edge!(S, 5, 7)
	add_edge!(S, 5, 8)
	add_edge!(S, 5, 9)
	
	add_edge!(S, 6, 7)
	add_edge!(S, 6, 8)
	add_edge!(S, 6, 9)
	
	add_edge!(S, 10, 11)
	add_edge!(S, 10, 12)
	add_edge!(S, 10, 13)
	add_edge!(S, 10, 14)
	
	add_edge!(S, 11, 12)
	add_edge!(S, 11, 13)
	add_edge!(S, 11, 14)
	
	add_edge!(S, 12, 13)
	add_edge!(S, 12, 15)
	
	add_edge!(S, 13, 15)
	
	add_edge!(S, 14, 16)
	add_edge!(S, 14, 17)
	add_edge!(S, 14, 18)
	
	add_edge!(S, 15, 16)
	add_edge!(S, 15, 17)
	add_edge!(S, 15, 18)
	
	S
end

# ╔═╡ 57a4c767-b476-43ae-a9f1-f4c52dd60740
begin
	nodelabel_0 = 1:nv(S)
	nodefillc_0 = fill(colorant"lightblue", nv(S))
	nodefillc_0[V_2] .= colorant"lightyellow"
	gplot(S, nodefillc=nodefillc_0, nodelabel=nodelabel_0)
end

# ╔═╡ f746048d-aed7-4b4c-974b-6052c4e4bbef
md"""
## Optimization
### Iteration 1
"""

# ╔═╡ c9584d35-d98d-48e0-a644-2290ff80130a
(c_1, V_1_1, V_2_1) = kernighan_lin(S, c_0, V_1, V_2)

# ╔═╡ 64d9731c-1a63-43b4-a1b2-d262e0f2d062
begin
	nodelabel_1 = 1:nv(S)
	nodefillc_1 = fill(colorant"lightblue", 18)
	nodefillc_1[V_2_1] .= colorant"lightyellow"
	gplot(S, nodefillc=nodefillc_1, nodelabel=nodelabel_1)
end

# ╔═╡ 408592e0-6f5a-4237-8e75-29e70b2b57e0
md"""
### Iteration 2
"""

# ╔═╡ efb33f07-e3df-436a-ba3e-63b529b510c5
(c_2, V_1_2, V_2_2) = kernighan_lin(S, c_1, V_1_1, V_2_1)

# ╔═╡ 0d7b21a6-46e4-4c96-b57b-e4691451acd7
begin
	nodelabel_2 = 1:nv(S)
	nodefillc_2 = fill(colorant"lightblue", 18)
	nodefillc_2[V_2_2] .= colorant"lightyellow"
	gplot(S, nodefillc=nodefillc_2, nodelabel=nodelabel_2)
end

# ╔═╡ Cell order:
# ╟─d9043bb8-0400-11ec-38b4-5dd261f49788
# ╟─0efecbfd-f255-4929-ac2c-1481a4ce8f8d
# ╟─c37d7cba-a331-42ff-bc54-a15d604737ec
# ╟─78a868e8-93b3-47a4-a03c-5c5267e841bf
# ╟─2b2a4d0c-76dd-4512-82f2-51f7d1c911bd
# ╟─62ad82f1-6ce3-4bdb-917d-e28f6ae2e049
# ╟─0473c25d-37b7-4285-abc6-221124d2bdb4
# ╟─62c3753e-189f-470e-bf83-861c441f2901
# ╟─57a4c767-b476-43ae-a9f1-f4c52dd60740
# ╟─f746048d-aed7-4b4c-974b-6052c4e4bbef
# ╠═c9584d35-d98d-48e0-a644-2290ff80130a
# ╟─64d9731c-1a63-43b4-a1b2-d262e0f2d062
# ╟─408592e0-6f5a-4237-8e75-29e70b2b57e0
# ╠═efb33f07-e3df-436a-ba3e-63b529b510c5
# ╟─0d7b21a6-46e4-4c96-b57b-e4691451acd7
