### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ c7287084-ec95-4428-9b28-a6ee4cb715be
begin
	import Pkg
	Pkg.activate(mktempdir())
	Pkg.add("GLMakie")
	Pkg.add(url="https://gitlab.com/griebel/smsi.jl")
	using GLMakie
	using SMSI
end

# ╔═╡ 88c94fcb-dd29-4b75-a0a4-1bcb603e5bb9
md"""
# Gustafson's Law

## Constants
"""

# ╔═╡ 783a9691-e9b0-4b10-8b60-5f5ade65c730
t_s = 8

# ╔═╡ 292e09c2-2a23-4ec3-a4ab-716ffac5dec3
md"""
## Variables
### N
"""

# ╔═╡ f8bb09df-cb1e-40eb-8e93-6a761bb0ed91
N_min = 1

# ╔═╡ c74f5eef-9099-4ff5-9156-6d64bfa091ee
N_max = 512

# ╔═╡ dedb999d-6fbc-487b-a912-45411b18ac4e
@bind N_slider html"<input type='range'>"

# ╔═╡ 7eb05669-eb32-4424-a2c4-eb78435b1440
N = Int(floor((N_max - N_min) / 100 * N_slider) + N_min)

# ╔═╡ b4629350-010d-4f4b-87a9-45a7a30f888a
t_p_min = 32

# ╔═╡ e1b2266f-c652-40eb-a00d-e58fd9fc7fbf
t_p_max = 512

# ╔═╡ f794519b-702c-4a0d-8fa5-78f36f0202c1
@bind t_p_slider html"<input type='range'>"

# ╔═╡ 55bd981c-fb48-4f53-801b-a48c9475cc09
t_p = Int(floor((t_p_max - t_p_min) / 100 * t_p_slider) + t_p_min)

# ╔═╡ ef884a42-1868-4037-8669-de6e6b0f7b13
md"""
## Result
"""

# ╔═╡ d4aeb0c7-0640-456a-99a2-d60513c32d60
S = gustafson_speedup(t_s, t_p, N)

# ╔═╡ aaea0521-e3ee-466a-801e-0138b04f2a86
begin
	xs = LinRange(N_min, N_max, 100)
	ys = LinRange(t_p_min, t_p_max, 100)
	zs = [gustafson_speedup(t_s, Int(floor(y)), Int(floor(x))) for x in xs, y in ys]
	
	surface(xs, ys, zs, axis=(type=Axis3,xlabel="t_p",ylabel="N",zlabel="S"))
end

# ╔═╡ Cell order:
# ╟─c7287084-ec95-4428-9b28-a6ee4cb715be
# ╟─88c94fcb-dd29-4b75-a0a4-1bcb603e5bb9
# ╟─783a9691-e9b0-4b10-8b60-5f5ade65c730
# ╟─292e09c2-2a23-4ec3-a4ab-716ffac5dec3
# ╟─f8bb09df-cb1e-40eb-8e93-6a761bb0ed91
# ╟─c74f5eef-9099-4ff5-9156-6d64bfa091ee
# ╟─dedb999d-6fbc-487b-a912-45411b18ac4e
# ╟─7eb05669-eb32-4424-a2c4-eb78435b1440
# ╟─b4629350-010d-4f4b-87a9-45a7a30f888a
# ╟─e1b2266f-c652-40eb-a00d-e58fd9fc7fbf
# ╟─f794519b-702c-4a0d-8fa5-78f36f0202c1
# ╟─55bd981c-fb48-4f53-801b-a48c9475cc09
# ╟─ef884a42-1868-4037-8669-de6e6b0f7b13
# ╠═d4aeb0c7-0640-456a-99a2-d60513c32d60
# ╟─aaea0521-e3ee-466a-801e-0138b04f2a86
